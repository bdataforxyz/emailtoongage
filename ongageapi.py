import requests
import time
import config
# import logging
import asyncio
# import aiohttp

log = config.log

now = time.time()
a_day = 86400
# now = datetime.now()
# today = now.strftime("%Y-%m-%d")
tomorrow = now + a_day
yesterday = now - a_day


def _url(path):
	log(f'CREATE path: {path}')
	# return 'https://httpbin.org/get'
	return 'https://api.ongage.net/api/' + path


def stop_sending(campaign_id):
	url_string = f'mailings/{campaign_id}/abort'
	log(f'STOP sending: {url_string}')
	url = _url(url_string)
	return url


class ApiOnage:

	def __init__(self, cred):
		self.cred = cred
		self.o_lid = cred['o_lid']
		self.o_user = cred['o_user']
		self.o_pass = cred['o_pass']
		self.o_code = cred['o_code']
		self.headers = {
			'X_USERNAME': self.o_user,
			'X_PASSWORD': self.o_pass,
			'X_ACCOUNT_CODE': self.o_code
		}

	async def start(self):
		log('starting')
		try:
			cid = await self.get_current_campaigns()
			if cid:
				self.parse_ongage_response(cid)
		except Exception as e:
			log('exception with start')
			log(e)

	async def get_current_campaigns(self):
		log('GET Current Campaigns')
		# cred = config.lid_cred[list_id)
		url = _url('mailings')
		param = {
			'date_from': yesterday,
			'date_to': tomorrow,
			# 'is_test': '0',
			'list_id': self.o_lid
		}
		cid = requests.get(url, params=param, headers=self.headers)
		await asyncio.sleep(0)

		log(f'GET: {cid}')
		data = cid.json()
		data = data['metadata']
		log(data)
		if data['error'] is False and int(data['total']) >= 1:
			log('there is running campaings')
			return cid
		return None

	def parse_ongage_response(self, resp):
		data = resp.json()
		log(f'GET data: {data}')
		params = {'list_id': self.o_lid}
		for campaign_item in data['payload']:
			# check to find the correct one by the status IN_PROGRESS status_desc or 60003 status
			status = campaign_item['status']
			log(f'GET status: {status}')
			if status == 60003:
				campaign_id = campaign_item['id']
				url_stop = stop_sending(campaign_id)
				requests.put(url_stop, headers=self.headers, params=params)
		return None
