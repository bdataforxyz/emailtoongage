from aioimaplib import aioimaplib
from datetime import datetime, timedelta
import logging
import config
import asyncio
import mailparser
import ongageapi

log = config.log


class EmailConnection:
	def __init__(self, cred):
		# todo make it so when an account dings that it has update. Add that account to a queue.
		# todo with digestor fetch unseen emails and add them to new queue to be proccessed process them.
		self.loop = asyncio.get_event_loop()
		self.parse_queue = asyncio.Queue(loop=self.loop)
		self.cred = cred
		self.e_user = cred['e_user']
		self.e_pass = cred['e_pass']
		self.e_imap = cred['e_conn']
		self.secret = cred['secret']
		self.imap_client = None
		self.mail_loop = None
		self.logged_in = False
		self.login_attempt = 0
		self.campaigns_stopped = 0

	async def login_smtp(self):
		n = 1
		log(f'USER: {self.e_user}      LOGIN')
		log(f'n: {self.login_attempt}')
		log(f'USER: {self.e_user}          imap_client: {self.imap_client}')
		self.imap_client = None
		while self.imap_client is None:
			try:
				self.imap_client = aioimaplib.IMAP4_SSL(host=self.e_imap, timeout=30)
				log(f'USER: {self.e_user}          imap_client: {self.imap_client}')
				hello = await self.imap_client.wait_hello_from_server()
				# log(f'hello: {hello}')
				login = await self.imap_client.login(self.e_user, self.e_pass)
				# self.logged_in = True
				# log(f'USER: {self.e_user}')
				log(f'      USER: {self.e_user} LOGIN: {login}')
				log(f'USER: {self.e_user}      LOGIN Success')
				select = await self.imap_client.select()
				# log(f'SELECT: {select}')
				# self.imap_client.has_capability("IDLE")
				log(f'USER: {self.e_user}          Login attempt #{self.login_attempt}')
				self.login_attempt += 1
				n += 1
				if n > 5:
					await asyncio.sleep(360)

					# config.wait_a_few(360, 360)
			# from code here.
			# https://github.com/home-assistant/home-assistant/blob/05ecc5a1355c7af11b5d310470a6171528dc7a2b/homeassistant/components/imap/sensor.py
			except (aioimaplib.AioImapException, asyncio.TimeoutError):
				log(f'USER: {self.e_user}          Log in Exception')
				log(f'USER: {self.e_user}          imap_client: {self.imap_client}')

				self.imap_client = None
		await self.unseen_emails()
		return self.imap_client

	async def idle_loop(self):
		self.imap_client = await self.login_smtp()
		while True:
			try:
				await self.unseen_emails()
				idle = await self.imap_client.idle_start()
				log(f'USER: {self.e_user}      IDLE started')
				log(f'USER: {self.e_user}      Server_Push: {await self.imap_client.wait_server_push()}')
				self.imap_client.idle_done()
				log(f'USER: {self.e_user}      IDLE Done')
				await self.unseen_emails()
				await asyncio.wait_for(idle, 30)
			# todo server returning ok still here. Need to soart that out so does not close idle on each ok message.
			except (aioimaplib.AioImapException, asyncio.TimeoutError):
				log(f'USER: {self.e_user}      EXCEPTION to re login')
				self.imap_client = None
				self.imap_client = await self.login_smtp()
				# await self.unseen_emails()
		return True

	async def archive_email(self, uid):
		# response = yield from imap_client.uid('STORE', uid, '+FLAGS', '\\Deleted')
		archive = await self.imap_client.uid('STORE', uid, '+FLAGS', '\\Seen')
		log(f'Archived: UID={uid} Response={archive}')
		if archive.result == 'ok':
			return True
		return False

	async def get_unseen_emails(self):
		log(f'USER: {self.e_user}      GET UNSEEN EMAILS')
		try:
			retcode, messages = await self.imap_client.uid_search('UNSEEN', charset=None)
			await asyncio.sleep(0)
			log(f'USER: {self.e_user}      Ret:{retcode}, MSG:{messages}')
		except (aioimaplib.AioImapException, asyncio.TimeoutError):
			retcode, messages = None, None
			log('USER: {self.e_user}      logging error in get_unseen_emails')
			await self.login_smtp()
		# 	todo make exception specific.
		# log(f'retcode: {retcode}')
		# log(f'messages: {messages}')
		if retcode == 'OK' and not messages[0] == "":
			log(f'USER: {self.e_user}      get list of uid.')
			uid_list = messages[0].split(' ')
			log(f'USER: {self.e_user}      uid list : {uid_list}')
			log(f'USER: {self.e_user}      GET UNSEEN EMAILS       Done')
			await asyncio.sleep(0)
			return uid_list
		log(f'USER: {self.e_user}      No UNSEEN')
		return None

	async def unseen_emails(self):
		# try:
		uid_list = await self.get_unseen_emails()
		log(f'USER: {self.e_user}      uid list : {uid_list}')
		if uid_list:
			await self.process_unseen_emails(uid_list)
			log(f'USER: {self.e_user}      EMAILS PROCCESED {uid_list}')
			return True
		log(f'USER: {self.e_user}      No EMAILS PROCCESED: {uid_list}')

	# except Exception as e:
		# 	log(e)

	async def process_unseen_emails(self, uid_list):
		log(f'PROCESS UNSEEN EMAILS')
		# await uid_list
		if uid_list != "" and uid_list:
			log(f'uid_list:{uid_list}')
			for n in uid_list:
				log(f'fetch email: {n}')
				await asyncio.sleep(0)
				response = await self.imap_client.uid('fetch', n, 'BODY.PEEK[HEADER]')
				await asyncio.sleep(0)
				if response.result == 'OK' and response.lines[1]:
					log(f'email fetched: {response}')
					try:
						log(f'handle response')
						get_email = await self.process_email(response)
						log(f'Email handled: {get_email}')
					except Exception as e:
						# todo make specific list index out of range
						print(e)
			return True

	async def process_email(self, response):
		log(f'Is response ok?: {response.result}')
		if response.result == "OK" and response.lines:
			log('response is ok')

			_mail, _uid = await self.get_parse_response(response.lines)
			# get_parse = await get_parse_response(response.lines)

			# _mail, _uid = get_parse[0], get_parse[1]
			try:
				log(f'mail:{_mail}')
				subject = _mail.subject
				m_from = _mail.from_[0]
				m_date = _mail.date
				m_from = m_from[1]
				log(f'from: {m_from}')
				log(f'subject: {subject}')
				log(f'secret: {self.secret.lower()}')
			except Exception as e:
				log(f'EXCEPTION         {e}')
			now = datetime.utcnow()
			log(m_date)
			log(now)
			if now-timedelta(minutes=260) <= m_date <= now+timedelta(minutes=5) and 'Ben@datafor.xyz'.lower() \
				in m_from.lower() and self.secret.lower() in subject.lower():
				log('Match')
				_api = ongageapi.ApiOnage(self.cred)
				await _api.start()
				log('Emergency stop run')
				# await asyncio.sleep(0)
			else:
				log('does not match')
				# log(m_from)
				# log(subject)

			await self.archive_email(_uid)
			log('Archived email')

			return True

	async def get_parse_response(self, response):
		log(f'PARSE content: {response}')
		_uid = response[0].split(' ')[3]
		await asyncio.sleep(0)
		_mail = mailparser.parse_from_bytes(response[1])
		await asyncio.sleep(0)
		return [_mail, _uid]


if __name__ == "__main__":
	test_cred = config.lid_cred['test']
	logging.debug(f'starting loop: {test_cred}')
	loop = asyncio.get_event_loop()
	loop.set_debug(enabled=True)
	logging.debug(f'wait for loop: {test_cred}')
	conn = EmailConnection(test_cred)
	# loop.run_until_complete(wait_for_new_message(test_cred))
	loop.run_until_complete(conn.idle_loop())
