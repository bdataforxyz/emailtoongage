import asyncio
from aioimaplib import aioimaplib
import config
import handle_email as handle
# import logging

log = config.log


class MailLogin:
	def __init__(self):
		self.loop = asyncio.get_event_loop()
		self.queue = asyncio.Queue(loop=self.loop)
		self.max_workers = len(config.active_lid) or 2

	async def produce_work(self):
		for lid in config.active_lid:
			cred = config.lid_cred[lid]
			await self.queue.put(cred)

	async def worker(self):
		while True:
			cred = await self.queue.get()
			log(f'{cred["e_user"]} Worker CREATED')
			# log(f'GOT creds: {cred}')
			while True:
				conn = handle.EmailConnection(cred)
				log(f'START idle loop {cred["e_user"]}')
				await conn.idle_loop()
				log(f'END idle loop {cred["e_user"]}')

	def start(self):
		try:
			self.loop.run_until_complete(
				asyncio.gather(self.produce_work(), *[self.worker() for _ in range(self.max_workers)], loop=self.loop,
								return_exceptions=True)
			)
		except Exception as e:
			log(f'Exception : {e}')
		finally:
			print('Done')


if __name__ == '__main__':
	MailLogin().start()
